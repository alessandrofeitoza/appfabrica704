import React from 'react';
import { Link } from 'react-router-dom';
import './style.css';

export default function AddressConfirmation() {
    return (
        <div className="containerAddress">
            <header className="header">
                <Link to=""> <i className="material-icons" color='#000'>arrow_back_ios</i></Link>
                <h1>ENDEREÇO DE ENTREGA</h1>
            </header>

            <form class="form-inline searchBar">
                <i class="material-icons">search</i>
                <input class="form-control form-control-sm ml-3 w-75" type="text" placeholder="Endereço e número"
                    aria-label="Search"/>
            </form>

            <div className="localizaçãoAtual">
                <i className="material-icons">gps_fixed</i>
                <div>
                    <h2>Usar localização atual</h2>
                    <p>Av. Filomeno Gomes - Centro, Fortaleza - CE</p>
                </div>
            </div>

            <ul className="addressContainer">
                <li>
                    <i className="material-icons">work</i>
                    <div>
                        <h2>Trabalho</h2>
                        <h3>Av. Filomeno Gomes - Centro, Fortaleza - CE</h3>
                        <p>Prédio Comercial - Sala 6</p>
                    </div>
                    <i className="material-icons points">more_vert</i>
                </li>

                <li>
                    <i className="material-icons">home</i>
                    <div>
                        <h2>Casa</h2>
                        <h3>Rua Quatrocentos Dezesseis - Conj São Cristovão, Fortaleza - CE</h3>
                        <p>Casa, 91</p>
                    </div>
                    <i className="material-icons points">more_vert</i>
                </li>

                <li>
                    <i className="material-icons">home</i>
                    <div>
                        <h2>Casa 2</h2>
                        <h3>Rua Afonso Lopes - Parque Dois Irmãos, Fortaleza - CE</h3>
                        <p>Casa, 1214</p>
                    </div>
                    <i className="material-icons points">more_vert</i>
                </li>
                
            </ul>

            <button>
                <Link to={'/pedido/confirmacao'}>Confirmar Endereço</Link>
            </button>
        </div>
    );
};