import React from 'react';
import './App.css';
import {Switch, Route, BrowserRouter, Router} from "react-router-dom";
import { createBrowserHistory } from 'history';
import Login from "./Login";
import "bootstrap/dist/css/bootstrap.min.css";
import Products from "./Products";
import Register from "./Register";
import Profile from "./Profile";
import Payment from "./Payment";
import ProductDetails from "./ProductDetails";
import Restaurant from "./Restaurant";
import OrderEvaluation from "./OrderEvaluation";
import ConfirmOrder from "./ConfirmOrder";
import Home from "./Home";
import PageNotFound from './PageNotFound';
import AddressConfirmation from './AddressConfirmation';

const history = createBrowserHistory();

function App() {
  return (
    <BrowserRouter>
      <Router history={history}>
        <Switch>
          <Route exact path={'/produtos'} component={Products} />
          <Route path={'/login'} component={Login} />
          <Route path={'/cadastro'} component={Register} />
          <Route path={'/meu-perfil'} component={Profile} />
          <Route path={'/restaurantes'} component={Restaurant} />
          <Route exact path={'/produto/detalhes'} component={ProductDetails} />
          <Route exact path={'/pedido/pagamento'} component={Payment} />
          <Route path={'/pedido/avaliacao'} component={OrderEvaluation} />
          <Route path={'/pedido/confirmacao'} component={ConfirmOrder} />
          <Route path={'/pedido/endereco'} component={AddressConfirmation} />
          <Route path={'/'} exact component={Restaurant} />
          <Route component={PageNotFound} />
        </Switch>
      </Router>
    </BrowserRouter>
  );
}

export default App;
