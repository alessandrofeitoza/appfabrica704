import React from "react";
import "./index.css";
import {Link} from "react-router-dom";

export default function ConfirmOrder(props) {
    return (
        <div id="confirmOrder" className="confirmOrder container-fluid ">
            <h4 className="text-center">Confirmação do Pedido</h4>
            <div className="card bg-light">
                <div className="card-body">
                    <p className="linhaItem d-flex justify-content-between ">
                        <h6>Produtos Comprados:</h6>
                        <a className="alterar" href="#">(Alterar)</a>
                    </p>
                    <p className="linhaItem d-flex justify-content-between ">
                        <label>01</label><label>Produto</label>
                        <label>R$ 12,00</label>
                    </p>
                    <p className="linhaItem d-flex justify-content-between">
                        <label>05</label><label>Produto</label>
                        <label>R$ 16,00</label>
                    </p>
                    <p className="linhaItem total d-flex justify-content-between font-weight-bold">
                        <label>Total</label>
                        <label>R$ 28,00</label>
                    </p>
                    <p className="linhaItem formaDeEntrega d-flex justify-content-start " >
                        <label className="font-weight-bold ">Forma de Entrega: </label>
                        <label><a className="alterar" href="#">(Alterar)</a></label>
                    </p>
                    <p className="linhaItem formaDeEntrega d-flex justify-content-aroun" >
                        <label className="font-weight-bold">Entregar</label>
                        <label className="">Rua Filomeno Gomes, 821</label>
                    </p>
                    <p className="linhaItem linhaAlterarPag d-flex justify-content-between">
                        <label><a className="alterar" href="#">(Alterar)</a></label>
                    </p>
                    <p className="linhaItem formaDePagamento d-flex justify-content-aroun" >
                        <label className="font-weight-bold ">Pagamento com: </label>
                        <label className="">Cartão de Crédito</label>
                    </p>
                    <form>
                        <button type="button" className="btn btn-success btn-lg btn-block">
                            <Link to={'/meu-perfil'}>Confirmar</Link>
                        </button>
                    </form>
                </div>
            </div>
        </div>
    );
}