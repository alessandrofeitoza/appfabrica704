import React from "react";
import {Link} from "react-router-dom";

export default function Login(props) {
    return (
        <div className ="container-fluid">
            <div className = "row">
                <div className = "col-md-12">
                    <form className ="mt-5 pb-3">
                        <label for ="email">Email</label>
                        <input id="email" className="form-control form-control-lg" type ="Email" placeholder ="Digite seu email"/>
                    
                        <label for ="senha">Senha</label>
                        <input id="senha" className ="form-control form-control-lg" type ="password" placeholder ="Digite sua senha"/>

                        <Link to={'/restaurantes'}>
                            <button className ="btn btn-primary btn-block mb-3 mt-2 btn-lg">Entrar</button>
                        </Link>
                        <span><a href ="#">Não possui cadastro?</a></span> <a className ="btn btn-link"><i className = {"material-icons"}>facebook</i></a> <a className ="btn btn-link"><i className = {"material-icons"}>attach_email</i></a>

                        <div className ="text-center">
                            <button type="button" class="btn btn-outline-primary">Esqueci minha senha</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    );
}