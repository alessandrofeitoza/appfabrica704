import React from "react";
import "./style.css";
import {Link} from "react-router-dom";

function backPage()
{
    alert('Ainda nao');
}

export default function OrderEvaluation(props) {
    return (
        <div className={'container-body'}>
            <nav className="navbar navbar-light bg-light center">
                <i className={"comeback material-icons"}>keyboard_arrow_left</i>
                <a className="navbar-brand text-center alignCenter" href="#">iFood</a>
            </nav>
            <div className="card md-3">
                <img src="/img/burguer.webp" className="card-img-top" alt="..." />
                    <div className="card-body">
                        <h5 className="card-title">Avalie seu pedido</h5>
                        <textarea className={'form-control mb-4'} placeholder={"Nos envie seu feedback"}></textarea>
                        <div className={"card evaluation mb-4 "}>
                            <div>
                                <i className={"material-icons col-sm-2"}>star_outline</i>
                            </div>
                            <div>
                                <i className={"material-icons col-sm-2"}>star_outline</i>
                            </div>
                            <div>
                                <i className={"material-icons col-sm-2"}>star_outline</i>
                            </div>
                            <div>
                                <i className={"material-icons col-sm-2"}>star_outline</i>
                            </div>
                            <div>
                                <i className={"material-icons col-sm-2"}>star_outline</i>
                            </div>
                        </div>
                        <div className={"center"}>
                            <button className="btn btn-outline-primary">
                                <Link to={'/login'}>Finalizar</Link>
                            </button>
                        </div>
                    </div>
            </div>
        </div>
    )
}