import React from 'react';
import './style.css';

export default function PageNotFound() {
    return(
        <div className="container-fluid page-not-found">
            <h1>Página Não Encontrada!!!!</h1>
            <img src={"/img/notFound.jpg"} alt="img"/>
            <h2>A página digitada não pode ser encontrada</h2>
            <button className="btn btn-danger btn-block btn-lg">Voltar para página inicial</button>
        </div>
    );
};