import React, { useState } from "react";
import { Link } from 'react-router-dom';
import "./styles.css";

export default function ProductDetails(props) {
    const [productPrice, setProductPrice] = useState(3.50);
    const [extraPrice, setExtraPrice] = useState(1.20);
    const [productDescription, setProductDescription] = useState("Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.");
    const [products, setProducts] = useState([
        {
            name: "Pastel de flango",
            quantity: 0,
        },
        {
            name: "Pastel de carne",
            quantity: 0,
        },
        {
            name: "Pastel de queijo",
            quantity: 0,
        },
    ]);

    const [extras, setExtras] = useState([
        {
            name: "Cheddar",
            quantity: 0
        },
        {
            name: "Requeijão",
            quantity: 0
        },
        {
            name: "Catupiry",
            quantity: 0
        },
    ]);

    const addValueAmount = (index) => {
        let newProducts = products.map((product, key: Number ) => {
            if (index === key) {
                product.quantity += 1; 
            }

            return product;
        });

        setProducts(newProducts);
    };

    const removeValueAmount = (index) => {
        let newProducts = products.map((product, key: Number ) => {
            if (index === key && product.quantity > 0 ) {
                product.quantity -= 1; 
            }

            return product;
        });

        setProducts(newProducts);
    };

    const addExtraValueAmount = (index) => {
        let newExtras = extras.map((extra, key: Number ) => {
            if (index === key && extra.quantity < 1) {
                extra.quantity += 1; 
            }

            return extra;
        });

        setExtras(newExtras);
    };

    const removeExtraValueAmount = (index) => {
        let newExtras = extras.map((extra, key: Number ) => {
            if (index === key && extra.quantity > 0 ) {
                extra.quantity -= 1; 
            }

            return extra;
        });

        setExtras(newExtras);
    };

    return (
        <div className="tela-detalhes" >
            <div className="header">
                <Link to="/produtos" ><i className="material-icons">reply</i> </Link>
                <h1 className="title" >Pastel</h1>
                <img 
                    src="https://portal-amb-imgs.clubedaana.com.br/2018/11/pastel-de-feira-1024x768.jpg" 
                    alt="Pastel" 
                />
            </div>
            
            <div>
            <ul className="list-group list-value">
                    <li className="list-group-item active">Valor <span>R$ {productPrice}</span></li>
                    {
                        products.map((product, index) => {
                            return (
                                <li key={index} className="list-group-item">
                                    {product.name}
                                    <button type="button" className="btn btn-outline-danger" onClick={() => removeValueAmount(index)} >
                                        <i className="material-icons" >remove</i>
                                    </button>
                                    <span>{product.quantity}</span> 
                                    <button type="button" className="btn btn-outline-danger" onClick={() => addValueAmount(index)} >
                                        <i className="material-icons" >add</i>
                                    </button>
                                </li>
                            );
                        })
                    }

                    <li className="list-group-item">{productDescription}</li>
                </ul>

                <ul className="list-group list-value">
                    <li className="list-group-item active">Valor por adicionais <span>R$ {extraPrice}</span></li>
                    {
                        extras.map((extra, index) => {
                            return (
                                <li key={index} className="list-group-item">
                                    {extra.name}
                                    <button type="button" className="btn btn-outline-danger" onClick={() => removeExtraValueAmount(index)} >
                                        <i className="material-icons" >remove</i>
                                    </button>
                                    <span>{extra.quantity}</span> 
                                    <button type="button" className="btn btn-outline-danger" onClick={() => addExtraValueAmount(index)} >
                                        <i className="material-icons" >add</i>
                                    </button>
                                </li>
                            );
                        })
                    }
                </ul>

                <button>
                    <Link to={'/pedido/endereco'}>Finalizar Pedido</Link>
                </button>
            </div>
        </div>
    );
}