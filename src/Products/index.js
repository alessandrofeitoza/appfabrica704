import React, {useState, useEffect} from "react";
import {Link} from "react-router-dom";

export default function Products(props) {
    const [products, setProducts] = useState([]);

    useEffect(() => {
        fetch('http://localhost:8000/products')
            .then(response => response.json())
            .then(response => {
                setProducts(response);
            });

        // outra forma de fazer a requisição
        /*let request = new XMLHttpRequest();
        request.open('get', 'http://localhost:8000/products');
        request.onload = () => {
            setProducts(JSON.parse(request.responseText));
        };
        request.send();*/
    }, []);

    if (products.length === 0) {
        return (
            <h1>Aguarde</h1>
        );
    }

    return (
        <div className={"container-fluid"}>
            {products.map(cadaProduto => (
                <div className="card">
                    <div className="card-body">
                        <div className="row">
                            <div className="col-7">
                                <h2>
                                    <Link to={'/produto/detalhes'}>
                                        {cadaProduto.name}
                                    </Link>
                                </h2>
                                <small>{cadaProduto.description}</small>
                            </div>
                            <div className="col-5 text-right">
                                <h4>R$ {cadaProduto.price}</h4>
                            </div>
                        </div>
                    </div>
                </div>
            ))}
        </div>
    );
}