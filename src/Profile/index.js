import React from "react";
import {Link} from "react-router-dom";

export default function Profile(props) {
    return (
        <div className="container">
            <div className="text-center">
                <img src="/img/usuario.png"  alt="..." className="img-circle"/> <p>Rodrigo Sousa</p>
            </div>

            <div className=" itens text-center d-flex justify-content-around mb-3" >
                <button type="button" class="btn btn-outline-primary">
                    <i className={"material-icons"}>chat</i>
                </button>
                <button type="button" class="btn btn-outline-primary">
                    <i className={"material-icons"}>phone</i>
                </button>
            </div>
            
            <ul className="list-group">
                <li className="list-group-item"> 
                    <i className={"material-icons"}>check</i> 
                    Cupons Disponíveis
                </li>
                <li className="list-group-item"> 
                    <i className={"material-icons"}>credit_card</i>
                     Cartões Cadastrados
                </li>
                <li className="list-group-item"> 
                    <i className={"material-icons"}>place</i>
                    Lugares favoritos</li>
                <li className="list-group-item"> 
                <i className={"material-icons"}>check_circle</i>
                    <Link to={'/pedido/avaliacao'}>Pedidos realizados</Link>
                 </li>
            </ul>  

            <div className ="mt-2"> 
                <button class="btn btn-outline-primary" role="alert">Editar  Perfil</button>

                <br>
                </br>

            </div>

            <div className ="text-center">
                <button class="btn btn-dark">voltar</button>
            </div>
        </div>
    );
}