import React from "react";
import "./styles.css";
import {Link} from "react-router-dom";

export default function Restaurant(props) {
    const ListRestaurants = (props) => {
        const {nameRestaurant, description, foodBusines} = props;
        
        return(
            <div className="containerRestaurants">         
                <div className="container">
                    <div className="row">
                        <div className="col-9">
                            <img src="https://upload.wikimedia.org/wikipedia/pt/b/bf/KFC_logo.svg" className="imgLogo"/>
                            <Link to={'/produtos'}>{nameRestaurant}</Link>
                            <div>
                                <br/>
                                {description}   
                            </div>
                        </div>
                        <div className="col-3">
                             {foodBusines}
                        </div>
                    </div>
                    <hr/>
                </div>
            </div>
        );
    };
    
    const NavBar = () => {
        return(
            <div className="navbar-restaurantes">
                <p>Inicio</p>
            </div>
        );
    }; 

    return (
        <>
            <NavBar/>
            {["Coco bambu", "Picanha do jonas", "Temaki"].map(cadaRestaurante => (
                <ListRestaurants nameRestaurant={cadaRestaurante} description={"Um bom restaurante"} foodBusines={"Brasileira"} />
            ))}
        </>
    );

}


